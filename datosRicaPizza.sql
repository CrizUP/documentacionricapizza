use sistemaricapizza;
insert into Empleado values(null, 'Raul', 'Perez Rodriguez', '2291206756', 'julio@gmail.com', 'Calle Julio Jaramillo #89 col. Primavera', 20980,  'Ciudad de México', 'rjulio', sha2('rjulio',256), 'EMPLEADO', 'ACTIVO');

insert into Empleado values(null, 'Marina', 'Ramirez Rodriguez', '2291216756', 'marina@gmail.com', 'Calle Jaramillo #9 col. Azueta', 10980,  'Veracruz', 'marinaRR', sha2('marinaRR',256), 'EMPLEADO', 'ACTIVO');

insert into Empleado values(null, 'Enrique', 'Mina Rodriguez', '2296126656', 'enri@gmail.com', 'Calle Avila  Camacho #89 col. Primavera', 91390,  'Xalapa', 'EriqueMR', sha2('EriqueMR',256), 'EMPLEADO', 'ACTIVO');

insert into Empleado values(null, 'Mario', 'Lopez Avila', '2281216756', 'marioL@gmail.com', 'Calle Primaver #89 col. Aguas Calientes', 20980,  'Coatzacoalcos', 'MarioLA', sha2('MarioLA',256), 'EMPLEADO', 'ACTIVO');

insert into Cliente values(null, 'Mario', 'Lopez Avila', '2281216756', 'marioL@gmail.com', 'Calle Primaver #89 col. Aguas Calientes', 20980,  'Coatzacoalcos', 'ACTIVO', 0);

insert into Cliente values(null, 'Gonzalo', 'Pérez Camacho', '2299216756', 'gonzalo@gmail.com', 'Calle Alcanfor #9 col. Flor Roja', 11980,  'México', 'ACTIVO', 0);

insert into pedido VALUES (1, 1, '2018-01-29', null, 'EN PROCESO');

insert into producto VALUES(null,'Pizza Hawaina','Pizza grande para 8 personas', 99.50,null, -1, 'ACTIVO',0);

INSERT INTO detallePedido VALUES (1,2, 1, null, 254.5 );
--TRIGGER para que se incrementen los pedidos del cliente automaticamente
Create trigger incrementarpedido after insert on pedido
for each row
update cliente set numeroPedidos = numeroPedidos+1 where idCliente = NEW.idCliente;
--TRIGGER para que se incremente el numero de vedidos en los productos automaticamente
Create trigger incrementarproducto after insert on detallePedido
for each row
update producto set numeroVendidos = numeroVendidos+NEW.cantidadPedida where codigoProducto = NEW.codigoProducto;

--Triger para que quite al stock la cantidad pedida
CREATE TRIGGER decrementrarstock after insert on detallePedido
FOR each ROW
UPDATE producto set cantidadStock = cantidadStock-NEW.cantidadPedida WHERE codigoProducto = NEW.codigoProducto;

--Para borrar un trigger
--DROP TRIGGER IF EXISTS nombre_trigger

--Trigger en conjunto  ya no se necesitan los individuales de arriba
Create trigger incrementarproducto after insert on detallePedido
for each row
update producto set numeroVendidos = numeroVendidos+NEW.cantidadPedida, 
cantidadStock = cantidadStock-NEW.cantidadPedida where codigoProducto = NEW.codigoProducto;